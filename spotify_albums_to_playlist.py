# Script that creates a Spotify playlist based on saved albums filtered by genre
# Get genre information from Discogs since Spotify genre coverage is poor

# Get id, secret, and redirect_url from Spotify developer page
# Get personal access token from Discogs -> My Avatar -> Settings -> Developer

import discogs_client
import spotipy
from spotipy.oauth2 import SpotifyOAuth
import random
import argparse  
import sys
import time
import string
import os
import re
import traceback
import json
from math import ceil
from urllib.error import HTTPError 


SPOTIFY_CLIENT_ID = str(os.environ.get('SPOTIFY_CLIENT_ID'))
SPOTIFY_CLIENT_SECRET = str(os.environ.get('SPOTIFY_CLIENT_SECRET'))
SPOTIFY_REDIRECT_URI = str(os.environ.get('SPOTIFY_REDIRECT_URI'))
DISCOGS_TOKEN = str(os.environ.get('DISCOGS_TOKEN'))

# sp = spotipy.Spotify(
#         auth_manager=
#         SpotifyOAuth(scope="user-library-read playlist-modify-public playlist-modify-private",
#                     client_id=SPOTIFY_CLIENT_ID,
#                     client_secret=SPOTIFY_CLIENT_SECRET,
#                     redirect_uri=SPOTIFY_REDIRECT_URI))




# Record = (title pattern, title replacement, artist pattern, artist replacement, year recorded, genre)
# Only title pattern is required 

REWRITE_DB = [ 
        ('dans la ville','Dans La Ville', 'Barney Wilen', 'Barney Wilen'),
        ('Cool Man Cool', 'Cool Man Cool', 'Herb Pilhofer Octet', 'John Plonsky'),
        ('Deep: The Classic 1980 New York Studio Session Remixed', 'Deep'),
        ('Barrelhouse Annie', 'Lil Johnson DOCD-5309'),
        ('Musical Prophet', 'Musical Prophet', 'Eric Dolphy', 'Eric Dolphy', 1963),
        ('Japanese Jazz Spectacle Vol.I','Japanese Jazz Spectacle Vol. I'),
        ('1917-36','The Complete Original Dixieland Jazz Band', 'Original Dixieland Jazz Band'),
        ('Masters Of The Boogie Woogie Piano','Boogie Woogie Piano B-1005', '','', 1924),
        ("Steve Angrum or George Lewis with Kid Sheik's Band","Steve Angrum George Lewis with Kid Sheik's Band"),
        ("Mr. Gavitt: Calypsos of Costa Rica","Calypsos - Afro-Limonese Music Of Costa Rica"),
        ("Fantastic Frank Strozier - Plus","Fantastic Frank Strozier"),
        ("Presenting Rare and Obscure Jazz Albums: Solo for Seven", "Solo for Seven"),
        ("Vaudeville Girl", "Here Comes Cleo", 'Cleo Brown'),
        ("Presenting Ray Bryant", "Ray Bryant Plays"),
        ('29th Street Saxophone Quartet', 'Pointillistic Groove'),
        ('ln Concert', 'The Koln Concert','Keith Jarrett'),
        ('bout de souffle', 'a bout de souffle', 'Martial Solal'),
        ('Anthology: The Deluxe Collection', 'The Complete Erskine Hawkins Volumes 1/2 (1938/1939)', 'Erskine Hawkins'),
        ('Velvet Underground & Nico', '', '', '', 1967),
        ('Here Comes Cleo', '', 'Cleo Brown', '', 1935),
        ('Jazz in Hollywood', '', 'Harry Babesin', '', 1955),
        ('Cruel Back Bitin', '', '', '', 1923 ),
        ('Best Of The Memphis Jug Band', '', '', '', 1927),
        ('Old Time Hokum Blues', '', '', '', 1935),
        ('Getz At The Gate', '', '', '', 1961),
        ('Death Might Be Your Santa Claus', '', '', '', 1949),
        ('The Centennial Collection', '', 'Robert Johnson', '', 1925),
        ('Both Directions At Once: The Lost Album','', '', '', 1963 ),
        ('R&r', '', 'Ruby Braff', '', 1955),
        ('Legendary Voices of Vaudeville','', '', '',  1920),
        ('The Legend of King Oliver', '', '', '', 1925),
        ('The Best Of Sidney Bechet', '', '', '', 1950),
        ('Killer Joe', '', '', '', 1956),
        ('Swing!', '', 'Frank Foster', '', 1956),
        ('Picture Of Heath', '', '', '', 1956),
        ('Anthology: The Definitive Collection', '', 'Rex Stewart', '', 1960),
        ('Earl Bostic Plays Sweet Tunes of the Roaring','', '', '', 1960 ),
        ('Anthology: The Definitive Collection', '', 'Charlie Shavers', '', 1963),
        ('Running the Gamut: Legendary Sessions', '', '', '', 1956),
        ('Blue Boogie: Boogie Woogie, Stride And The Piano Blues', '', '', '', 1924),
        ('The Trumpet Summit Meets The Oscar Peterson Big Four', '', '', '', 1980),
        ('Grand Encounter', '', 'John Lewis', '', 1956),
        ('Haunting Melodies', '', '', '', 1989),
        ('You Get More Bounce With Curtis Counce', '', '', '', 1957),
        ("Live at Ronnie Scott's 61", '', '', '', 1961),
        ("Hamburg '72", '', 'Keith Jarrett', '', 1972),
        ("The Bristol Sessions", '', '', '', 1927),
        ('The Art Tatum Solo Masterpieces','', '', '', 1974 ),
        ('First Impulse: The Creed Taylor Collection 50th Anniversary', '', '', '', 1961),
        ('Presenting Eddie Condon', '', '', '', 1927),
        ('The Complete Blue Note Recordings', '', 'Thelonious Monk', '', 1965),
        ('Four Horns And A Lush Life', '', '', '', 1956),
        ('Verve Jazz Masters 43: Ben Webster', '', '', '', 1959),
        ('Bix Beiderbecke 20 Classic Tracks', '', '', '', 1925),
        ('The Best of Mississippi John Hurt', '', '', '', 1971),
        ('The Best of Memphis Jug Band', '', '', '', 1940),
        ('The Best of The Carter Family', '', '', '', 1935),
        ('good 4 u', "good 4 u / Dooley's Farm", 'Molly Tuttle','Molly Tuttle & Golden Highway', 2023, '#alternative folk#folk#alternative#')
    ]

def rewriter(artist:str, title:str)->tuple[str,str,int,str]:
    for (record) in REWRITE_DB:
        title_pattern = record[0]
        title_replacement = record[1] if len(record)>1 and record[1] else title
        artist_pattern = record[2] if len(record)>2 else ''
        artist_replacement = record[3] if len(record)>3 and record[3] else artist
        year = record[4] if len(record)>4 else 0
        genre = record[5] if len(record)>5 else ''

        if re.search(title_pattern, title) and re.search(artist_pattern, artist):
            return (artist_replacement, title_replacement, year, genre)
    
    return (artist, title, 0, '')



# Use L and D as needed to explicitly typecheck responses 
def L(obj)->list:
    if not isinstance(obj,list):
        obj = []
    assert(isinstance(obj,list))
    return obj

def D(obj)->dict:
    if not isinstance(obj,dict):
        obj = { }
    assert(isinstance(obj,dict))
    return obj

def S(obj)->str:
    if not isinstance(obj,str):
        obj = ''
    assert(isinstance(obj,str))
    return obj

def any_in(items:list[str], s:str):
    if len(items)==0:
        return True
    for item in items:
        if item in s:
            return True
    return False

def any_none(good:list[str], bad:list[str], s:str, exact:bool=False):
    okay = False
    if len(good)==0:
        okay = True
    else:
        for item in good:
            if exact:
                item = '#' + item + '#'
            if item in s:
                okay = True
                break
    if not okay:
        return False
    for item in bad:
        if exact:
            item = '#' + item + '#'
        if item in s:
           return False
    return True

def albums_to_playlist(
        spotify_client_id:str,
        spotify_client_secret:str,
        spotify_redirect_uri:str,
        discogs_token:str,
        playlist_name:str = '',
        genre:list[str] = [],
        notgenre:list[str] = [],
        artists:list[str] = [],
        titles=[],
        exact_match:bool = False,
        startyear:int = 0,
        endyear:int = 3000,
        in_datafile:str='',
        out_datafile:str='',
        split:int=1,
        shuffled:bool=False,
        new:bool=False,
        extend:bool=False
        ):
    
    # declare variables used by discosearch
    d = None
    total_requests = 0
    last_time = time.time()

    # Search Discogs
    def discosearch(*positional, **keyword):
        nonlocal d, total_requests, last_time
        # Sleep for 2 seconds between Discogs searches, otherwise Discogs will throttle
        time.sleep(2)

        # Every 30 requests print the request rate
        total_requests += 1
        now = time.time()
        if total_requests % 30 == 0:
            delta = now - last_time
            last_time = now
            rate = round(30 / (delta/60))
            print(f"Discogs rate = {rate} requests per minute")
        
        # Try the request.  If the request times out, then retry with a new client; if
        # it still times out, then retry with no timeout set.
        try:
            if not d:
                d = discogs_client.Client('spotify_albums_to_playlist_v1', user_token=discogs_token)
                print("Created Discogs client")
                d.set_timeout(connect=15, read=15)
            return d.search(*positional,**keyword)
        except TimeoutError:
            try:
                d = discogs_client.Client('spotify_albums_to_playlist_v1', user_token=discogs_token)
                d.set_timeout(connect=15, read=15)
                print("Warning: timeout error, recreating client")
                return d.search(*positional,**keyword)
            except TimeoutError:
                d = discogs_client.Client('spotify_albums_to_playlist_v1', user_token=discogs_token)
                print("Warning: double timeout error, eliminating timeouts")
                return d.search(*positional,**keyword)


    # Create Spotify client
    scope = "user-library-read playlist-modify-public playlist-modify-private"
    sp = spotipy.Spotify(
        auth_manager=
        SpotifyOAuth(scope=scope,
                    client_id=spotify_client_id,
                    client_secret=spotify_client_secret,
                    redirect_uri=spotify_redirect_uri))

    #
    # Set up album database
    #

    albums_db = []
    albums_in_db = set()
    new_albums = []
    if in_datafile:
        # Load albums database from file
        with open(in_datafile,'r') as f:
            albums_db = json.loads(f.read())
        for a in albums_db:
            albums_in_db.add(a['album_id'])
        print(f"Read album database {in_datafile}")
    
    if not albums_db or out_datafile or new:
        # Create or update albums database from Spotify
    
        # Get Spotify saved albums
        spotify_albums = []
        offset = 0
        done = False

        while not done: 
            response = D(sp.current_user_saved_albums(limit=50, offset=offset))
            items = response['items']
            if len(items)<50:
                done = True
            spotify_albums += items
            offset += len(items)
            print(f"Downloaded {offset} albums...")
        print(f"Found {len(spotify_albums)} saved albums")

        # Create or expand database of albums combining data from Spotify and Discogs
        original_title = ''
        original_artist = ''
        for item in spotify_albums:
            try:
                album = (item)['album']
                album_id = album['id']
                if album_id not in albums_in_db:
                    new_albums.append(album_id)
                    album_title = album['name']
                    primary_artist_name = album['artists'][0]['name']
                    original_title = album_title
                    original_artist = primary_artist_name

                    # Look up the album genres on Discogs
                    #   Covert title and artist to ascii
                    #   Remove material in parenthesis from title
                    #   Drop substrings in () or [] in title
                    #   Drop artists named "various artists"
                    #   Rewrite title and artist
                    # Search for albums trying:
                    # 1. match artist and title, type=master
                    # 2. Drop type=master
                    # 3. general search of artist + ' ' + title
                    # 4. general search of title
                    # 5. shorten title

                    album_title = album_title.encode('ascii','ignore').decode('ascii')
                    primary_artist_name = primary_artist_name.encode('ascii','ignore').decode('ascii')
                    
                    # If artist is something like "Various Artists" then ignore it
                    if re.search('artists', primary_artist_name, flags=re.I):
                        primary_artist_name = ''
                    
                    # Handle special cases using rewrite database


                    (primary_artist_name, album_title, year_recorded,genres_string) = rewriter(primary_artist_name,album_title)

                    # Determine genres if not already set by rewriter
                    if genres_string == '' or year_recorded <=0:
                        # Eliminate material in ()'s or []'s from title
                        album_title = re.sub('\\(.*\\)', '', album_title).strip()
                        album_title = re.sub('\\[.*\\]', '', album_title).strip()

                        # Search Discogs from most to least specific
                        results = discosearch(title=album_title, type='master', artist=primary_artist_name)
                        if (results.count==0):
                            results = discosearch(type='release', title=album_title, artist=primary_artist_name)
                            if results.count==0:
                                results = discosearch(primary_artist_name + ' ' + album_title)
                                if results.count==0:
                                    results = discosearch(album_title)
                                    if results.count==0:
                                        album_title = ' '.join((album_title.split())[:10])
                                        if results.count==0:
                                            print(f"No Discogs match, skipping {original_artist}, {original_title} as {primary_artist_name}, {album_title}")
                                            continue
                        
                        # The length of page(0) is more accurate than the number in the count attribute
                        if len(results.page(0)) == 0:
                            print(f"Discogs returned empty page, skipping {original_artist}, {original_title} as {primary_artist_name}, {album_title}")
                            continue
                        num_results = len(results.page(0))

                        # Find a result that includes a title to know we are not getting an artist or something else
                        first_album_index = -1
                        for i in range(num_results):
                            if hasattr(results[i],'title'):
                                first_album_index = i
                                break
                        if first_album_index < 0:
                            print(f"Discogs only found non-albums, skipping {original_artist}, {original_title} as {primary_artist_name}, {album_title}")
                            continue                    

                        # Get all the genres. Discogs makes an arbitrary separation between genres and styles, so use both.
                        discogs_title = results[first_album_index].title
                        album_genres = L(getattr(results[first_album_index], 'genres', []))
                        album_styles = L(getattr(results[first_album_index], 'styles', []))
                        album_genres += album_styles 
                        if len(album_genres) == 0:
                            print(f"Warning: No genres found for {original_artist}, {original_title} as {primary_artist_name}, {album_title}")
                        
                        if genres_string == '':
                            genres_string = '#' + ('#'.join(album_genres)).casefold() + '#'

                    # Determine the year recorded if it was not already manually set by rewriter()
                    if year_recorded <= 0:
                        
                        # Get earliest year by searching first 5 results
                        year_recorded = 2999
                        for i in range(first_album_index,first_album_index+ min(num_results,5)): #type:ignore 
                            if hasattr(results[i],'year'): #type:ignore
                                year = int(results[i].year) #type:ignore
                                if year > 0 and year < year_recorded:
                                    year_recorded = year

                        # Overide Discogs year if Spotify album name contains a year
                        # Uses Spotify name because year might be in ()'s which was deleted for Discog search
                        match_obj = re.search("(19|20)[0-9][0-9]", original_title)
                        if match_obj:
                            year = int(match_obj.group(0))
                            if year < year_recorded:
                                year_recorded = year
                        
                        # Also check Discogs title for a year
                        # Sometimes Discogs title contains a year when Spotify title does not
                        match_obj = re.search("(19|20)[0-9][0-9]", discogs_title) #type:ignore 
                        if match_obj:
                            year = int(match_obj.group(0))
                            if year < year_recorded:
                                year_recorded = year

                    # Add album to database
                    albums_db.append({
                        'album_title': original_title,
                        'album_artist': original_artist,
                        'album_year': year_recorded,
                        'album_id': album['id'],
                        'track_ids': [ str(track['id']) for track in album['tracks']['items'] ],
                        'genres_string': genres_string
                        })

                    # Show progress
                    print('+',end='',flush=True)
                else:
                    print('-',end='', flush=True)
            
            
            except (HTTPError, AssertionError, AttributeError, IndexError, KeyError, NameError, RuntimeError, ValueError ) as err:
                # Do not want to catch control-C interrupt
                print(f"Discogs API error, skipping {original_artist}, {original_title}")
                print(f"{type(err).__name__} was raised: {err}")
                traceback.print_exc()
                continue
        print()

    #
    # Sort the album database by date
    #
    albums_db.sort(key=lambda a: a['album_year'])


    #
    # Save the albums database to a file
    #
    if out_datafile:
        with open(out_datafile, 'w') as f:
            f.write(json.dumps(albums_db, indent=2))
            print(f"Wrote album database {out_datafile}")

    #
    # Filter albums by genre and year and save tracks
    #
    all_tracks = []
    num_matching_albums = 0
    for a in albums_db:
        genres_string = a['genres_string']
        track_ids = a['track_ids']
        album_year = a['album_year']
        album_title = a['album_title']
        album_artist = str(a['album_artist'])
        album_id = a['album_id']

        # If album matches genre, year range, and artists, then add its tracks to playlist
        if ( any_none(genre, notgenre, genres_string, exact_match) and
            startyear <= album_year and album_year <= endyear and
            any_none(artists, [], album_artist.casefold(), exact_match) and
            any_none(titles, [], album_title.casefold(), exact_match) and
            ((not new) or album_id in new_albums)
            ):
                num_matching_albums += 1
                print(f"Adding {len(track_ids)} tracks from {album_artist}, {album_title} ({album_year}): {genres_string}")
                for id in track_ids:
                    all_tracks.append((id,album_id))

    print(f"Number of albums is {num_matching_albums}")
    print(f"Total number of tracks is {len(all_tracks)}")

    #
    # Create Spotify playlist
    #
    if playlist_name:

        if shuffled:
            # Shuffle tracks
            random.shuffle(all_tracks)
        
        # Get user id - not the same as client id
        profile = D(sp.me())
        my_id = profile['id']

        if split < 1:
            split = 1

        total_tracks = len(all_tracks)
        if total_tracks / split > 9900: # Add a little slop to allow for uneven split
            split = ceil(total_tracks/9900)
            print(f"Warning: Setting {split=} due to 10,000 track playlist limit")
        split_size = ceil(total_tracks/split)

        offset=0
        for playlist_number in range(1,split+1):

            if split == 1:
                playlist_split_name = playlist_name
            else:
                playlist_split_name = f"{playlist_name} {playlist_number:0{len(str(split))}d}"

            #
            # Create or update playlist
            #
            my_playlists = []
            my_playlists_offset = 0
            print('Getting user playlists ... ')
            while True:
                p = D(sp.current_user_playlists(offset=my_playlists_offset))
                items = p['items']
                if not items:
                    break
                my_playlists += items
                my_playlists_offset += 50
            playlist_id = None
            for playlist in my_playlists:
                if playlist['name'] == playlist_split_name:
                    playlist_id = playlist['id']
                    if extend:
                        print(f"Extending playlist {playlist_split_name}")
                    else:
                        sp.playlist_replace_items(playlist_id,[])
                        print(f"Replacing playlist {playlist_split_name}")
                    break
            if not playlist_id:
                playlist = D(sp.user_playlist_create(user=my_id, name=playlist_split_name))
                playlist_id = playlist['id']
                print(f"Creating playlist {playlist_split_name}")

            # Add tracks to playlist in chunks
            remaining_this_split = min(split_size, total_tracks-offset)
            size_this_split = remaining_this_split

            # for unshuffled playlist, don't split an album
            if not shuffled:
                # Possibly enlarge the current split in order to finish off an album
                last_position = offset + remaining_this_split - 1
                last_album = all_tracks[last_position][1]
                while last_position + 1 < len(all_tracks) and last_album == all_tracks[last_position+1][1]:
                    last_position += 1
                remaining_this_split = last_position - offset + 1


            while remaining_this_split > 0:
                chunksize = min(50, remaining_this_split)

                chunk = [ id for (id,_) in all_tracks[offset:offset+chunksize] ]
                sp.playlist_add_items(playlist_id=playlist_id, items=chunk)
                remaining_this_split -= chunksize
                offset += chunksize
                print(f".", flush=True, end='')
            print(f"\nAdded {size_this_split} tracks to playlist {playlist_split_name}")


def main():
    parser = argparse.ArgumentParser(description="Create playlist from saved albums filtered by genre, artists, title, and/or year")
    parser.add_argument('--save', '-s', action='store_true', help='Save playlist to Spotify, otherwise is a dry run')
    parser.add_argument('--name', '-n', type=str, default='', help='Name of playlist, if omitted name is created from genre')
    parser.add_argument('--genre', '-g', action='extend', default=[], type=str, nargs='+', help='One or more genres to include')
    parser.add_argument('--except', '-x', dest='notgenre', metavar='GENRE', action='extend', default=[], type=str, nargs='+', help='One or more genres to exclude')
    parser.add_argument('--artists', '-a', action='extend', default=[], type=str, nargs='+', help='One or more artists to include')
    parser.add_argument('--title', '-t', action='extend', default=[], type=str, nargs='+', help='One or more (partial) titles to include')
    parser.add_argument('--exact', '-e', action='store_true', help='Match filters exactly, otherwise substring match permitted')
    parser.add_argument('--years', '-y', type=int, metavar=('START_YEAR','END_YEAR'), default=[0,1000000], nargs=2, help='Only match albums between pair of years inclusive')
    parser.add_argument('--read', '-r', type=str, default='', metavar='FILE', help='import album database from a file')
    parser.add_argument('--write', '-w', type=str, default='', metavar='FILE', help='export database of all saved albums with genres to a file')
    parser.add_argument('--update', '-u', type=str, default='', metavar='FILE', help='equivalent to --read FILE --write FILE')
    parser.add_argument('--split', '-p', type=int, default=1, help='Create N numbered playlists')
    parser.add_argument('--shuffle', '-S', action='store_true', help='Shuffle playlist') 
    parser.add_argument('--new', '-N', action='store_true', help='Only include albums not already in local database')
    parser.add_argument('--extend', '-E', action='store_true', help='If playlist already exits, add to it rather than replace it.\n'
                                                                'Warning: this option can result in repeated tracks in playlist')

    
    args = parser.parse_args()
    save:str = args.save
    genre:list[str] = args.genre
    notgenre:list[str] = args.notgenre
    titles:list[str] = args.title
    name:str = args.name 
    artists = args.artists
    genre = [ s.casefold() for s in genre ]
    notgenre = [ s.casefold() for s in notgenre]
    artists = [ s.casefold() for s in artists]
    titles = [ s.casefold() for s in titles]
    split = args.split if args.split >= 1 else 1


    if not name:
        if args.shuffle: 
            name = 'Shuffled'
        else:
            name = 'My'
        if titles:
            name += ' ' + string.capwords(' '.join(titles))
        if genre:
            name += ' ' + string.capwords(' '.join(genre))
        name += ' Albums'
        if artists:
            name += ' By ' + string.capwords(' '.join(artists))
        if args.years[0]>0:
            name += ' ' + str(args.years[0]) + '-' + str(args.years[1])
    if not save:
        name = ''
 
    in_datafile = args.read
    out_datafile = args.write
    if args.update:
        in_datafile = args.update
        out_datafile = args.update
    
    albums_to_playlist(
        SPOTIFY_CLIENT_ID,
        SPOTIFY_CLIENT_SECRET,
        SPOTIFY_REDIRECT_URI,
        DISCOGS_TOKEN,
        playlist_name=name,
        genre=genre,
        notgenre=notgenre,
        artists=artists,
        titles=titles,
        exact_match=args.exact,
        startyear=args.years[0],
        endyear=args.years[1],
        in_datafile=S(in_datafile),
        out_datafile=S(out_datafile),
        split=split,
        shuffled=args.shuffle,
        new=args.new,
        extend=args.extend
        )

if __name__ == '__main__':
    main()

