# spotify\_albums\_to\_playlist.py

Create a Spotify playlist from saved albums filtering by genres, artist, title, and/or years.  Genres are retrieved
from Discogs because Spotify genres are spotty.

## Use

To create a playlist of tracks in chronological order of saved blues albums that are not also jazz albums that were
first released between 1930 and 1950:

    python spotify_albums_to_playlist.py --ordered --genre blues --except jazz --years 1930 1950 --save

To create a local database of saved albums:

    python spotify_albums_to_playlist.py --write albumsdb.json

It can take an hour or more to create this local database, so it is best to write it to a file
if more than one playlist is to be created.

To use the local database to create a shuffled playlist of tracks from saved punk or alternative albums:

    python spotify_albums_to_playlist.py --read albumsdb.json --genre punk alternative --save

To use the local database to create a playlist named My Favs of tracks from saved albums in chronological by Miles Davis or Dizzy Gillepsie:

    python spotify_albums_to_playlist.py --read albumsdb.json --name 'My Favs' --artists miles dizzy --ordered --save

If you specify both a read and write database, then Discogs will be checked only for albums that are not in
the read database.  If the same file is specified for both read and write databases, then the database is
updated with new saved albums, whether or not a playlist is created:

    python spotify_albums_to_playlist.py --read albumsdb.json --write albumsdb.json

If you create a playlist with the name of one of your existing playlists, the existing playlist is replaced.

To see all options, execute

    python spotify_albums_to_playlist.py --help

## Required Python packages

    pip install spotipy python3_discogs_client argparse

The home of spotipy is https://github.com/spotipy-dev/spotipy.

The home of python3_discogs_client is https://github.com/joalla/discogs_client.

## Getting Spotiy and Discogs credentials 

* Follow [these steps](https://developer.spotify.com/documentation/general/guides/authorization/app-settings/) to
create an app and get a client ID, a client secret, and redirect URI (an arbitrary URL).  

* On the Discogs web page, login and click on your icon at the top right, then select Settings, Developers, Generate New Token.  Set shell variable to the token string.

* In your .profile or .bashrc set shell variables, where "some string" is replaced with the values for your credentials:

    export SPOTIFY_CLIENT_ID="some string"
    export SPOTIFY_CLIENT_SECRET="some string"
    export SPOTIFY_REDIRECT_URI="http://localhost:8227/"
    export DISCOGS_TOKEN="some string"

## Matching Spotify albums to Discogs albums

There is no universal code for record albums. The program matches Spotify albums to Discogs albums using the following heuristics in order:

1. The Spotify first artist name and album title are converted to ascii, because unicode characters do not always match correctly between the Spotify and Discogs.

2. The artist and title are matched against the program's internal rewrite database (see below) and modified if appropriate.  If the rewrite database also contains the year or genres, those values are used instead of 
checking Discogs.

3. Substrings in the title that are in parentheses or square brackets are removed from the title.

4. Discogs is searched for a master release using the artist and title search fields.

5. If that search fails, Discogs is searched for any release using the artist and title search fields.

6. If that search fails, Discogs is searched with a simple search string of the artist name concatenated 
with the title.

7. If that search fails, Discogs is searched with a simple search string of the title.

8. If that search fails, Discogs is searched with a simple search string of the first 10 words of the title.


## Album years

The program attempts to determine the year the album was recorded rather than the years of later releases.  It uses the following heuristics in order:

1. If the year is specified in the program's internal rewrite database, that year is used.
2. If a year appears in the title of the album (that is, a 4-digit number beginning 19 or 20), that year is used.  Both the title from Spotify and the title from the corresponding Discog album are considered.
3. If an Discogs master release is found, then its year is used.
4. Otherwise, the first five releases returned by the Discogs search API are examined, and the earliest year is used.

## Fixing lookup errors

Once in a while, a Spotify album cannot be found easily on Discogs because its name or the name of its artist differs between the two sites.  There are also problems when the earliest release date in Discogs is years later than the recording date, which is common for compilations.

You can edit the list named REWRITE\_DB in spotify\_albums\_to\_playlist.py to manually specify how an album title and/or artist should be rewritten in order to be found in Discogs and to optionally specify a recording data.  The format of each record is:

    (title pattern, title replacement, artist pattern, artist replacement, year recorded, genre string)

Only "title pattern" is required. If a pattern field is the empty string, it matches everything. If
a replacement field is the empty string, the title or artist is unchanged. If the year is included, it overrides the year listed in Discogs.  If the genre string is included, it overrides genre information listed in Discogs.  Including both year recorded and genre string means that Discogs is not checked at all for the album.




 